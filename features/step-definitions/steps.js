import { Given, When, Then } from "@wdio/cucumber-framework";
import { expect } from "chai";

Given("I am on page with button Add Element", async () => {
  await browser.url(`https://the-internet.herokuapp.com/add_remove_elements/`)
});

When("I click on button Add Element", async () => {
  const button = await $('#content > div > button')
  await button.click()
  
});

Then("I see that button Delete appear", async () => {
  const button = await $('#elements > button:nth-child(1)')
  const isButtonDisplayed = await button.isDisplayed()
  expect(isButtonDisplayed).to.be.true
});

When("I click on button Delete", async () => {
  const button = await $('#elements > button:nth-child(1)')
  await button.click()
  
});

Then("I see that button Delete disappeared", async () => {
  const button = await $('#elements > button:nth-child(1)')
  const isButtonDisplayed = await button.isDisplayed()
  expect(isButtonDisplayed).to.be.false

});