Feature: Add/Remove Elements

  Scenario: Add and Remove Elements

    Given I am on page with button Add Element
    When I click on button Add Element
    Then I see that button Delete appear 
    When I click on button Delete
    Then I see that button Delete disappeared

